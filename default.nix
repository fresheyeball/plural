{ compiler ? "default", doBenchmark ? false }:

let
  rev = "ee28e35ba37ab285fc29e4a09f26235ffe4123e2";

  pkgs = import
      (builtins.fetchTarball { url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";     }) {};

  f = { mkDerivation, base, containers, stdenv, hspec, bytestring }:
      mkDerivation {
        pname = "Plural";
        version = "0.0.2";
        src = ./.;
        libraryHaskellDepends = [ base containers bytestring ];
        testHaskellDepends = [ base hspec ];
        description = "Pluralize English words";
        license = stdenv.lib.licenses.gpl3;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
